@if(session('success'))
    <div class="row">
        <div class="col">
            <div class="alert alert-success" role="alert">
                {{session('success')}}
            </div>
        </div>
    </div>
@endif

@if(session('error'))
    <div class="row">
        <div class="col">
            <div class="alert alert-danger" role="alert">
                {{session('error')}}
            </div>
        </div>
    </div>
@endif

@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
