@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col">
            Create product
        </div>
    </div>
    @include('alerts')
    <div class="row">
        <div class="col">
            <form enctype="multipart/form-data" method="post" action="{{route('admin.products.store')}}">
                @csrf
                <div class="form-group">
                    <label for="t">Title</label>
                    <input type="text" class="form-control" id="t" placeholder="Product title" name="title">
                </div>
                <div class="form-group">
                    <label for="p">Price</label>
                    <input type="number" class="form-control" id="p" placeholder="Price" name="price">
                </div>
                <div class="form-group">
                    <label for="d">Description</label>
                    <textarea class="form-control" id="d" rows="3" name="desc"></textarea>
                </div>
                <label for="res">Choose a res:</label>
                <select class="form-control" name="restaurant_id" id="res">
                    @foreach($res as $r)
                        <option value="{{$r->id}}">{{$r->title}}</option>
                    @endforeach
                </select>
                <br>
                <div class="form-group">
                    <label for="pic">Picture</label>
                    <input type="file" class="form-control-file" id="pic" name="pic">
                </div>
                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
@endsection
