@extends('layouts.admin')
@section('componentcss')
    <style>
        .cut-text {
            text-overflow: ellipsis;
            overflow: hidden;
            width: 160px;
            height: 1.2em;
            white-space: nowrap;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col">
            <h3>Products <a href="{{route('admin.products.create')}}" class="btn btn-primary btn-sm
  active" role="button" aria-pressed="true">Create new Product</a></h3>
        </div>
    </div>
    @include('alerts')
    <div class="row">
        @foreach($products as $product)
            <div class="col" style="padding: 35px 0 0 0;">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="{{$product->pic}}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{$product->title}}</h5>
                            <p class="cut-text">
                                {{ $product->desc }}
                            </p>
                        <a href="{{route('admin.products.show', ['product' => $product])}}" class="card-link btn btn-sm btn-outline-info">
                            Show
                        </a>
                        <a href="{{route('admin.products.edit', ['product' => $product])}}" class="card-link btn btn-sm btn-outline-primary">Edit</a>
                        <form style="display: inline-block" class="card-link" action="{{route('admin.products.destroy', compact('product'))}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                        </form>
                        <br>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row p-5">
        <div class="col col-md-6 offset-md-3">
            {{ $products->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection
